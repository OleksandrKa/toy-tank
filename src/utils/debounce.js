export default function debounce(callback, wait, immediate = false) {
  let timeout = null;

  return function debounceFn(...args) {
    const callNow = immediate && !timeout;
    const next = () => callback.apply(this, args);

    clearTimeout(timeout);
    timeout = setTimeout(next, wait);

    if (callNow) {
      next();
    }
  };
}
