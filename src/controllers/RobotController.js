/* eslint-disable */

import simple_jsonrpc from "../vendors/simple-jsonrpc-js.js";
import Deferred from "../utils/Deferred";

const defaultWebSocketHost = window.location.hostname || "localhost";
const defaultWebSocketPort = 8765;

export default class RobotController {
  untilWebSocketReady = new Deferred();

  logs = [];

  constructor({ webSocketHost = defaultWebSocketHost, webSocketPort = defaultWebSocketPort }) {
    this.ws = new WebSocket(`ws://${webSocketHost}:${webSocketPort}`);
    this.ws.addEventListener("close", ev => this.wsOnclose(ev));
    this.ws.addEventListener("error", ev => this.wsOnclose(ev));
    this.ws.addEventListener("message", ev => {
      // console.log(ev.data);
      this.jrpc.messageHandler(ev.data);
    });
    this.ws.addEventListener("open", ev => this.wsOnOpen(ev));

    // json-rpc client setup
    this.jrpc = new simple_jsonrpc();
    this.jrpc.toStream = msg => {
      if (this.ws !== null && this.ws.readyState === this.ws.OPEN) {
        // console.log(msg);
        this.ws.send(msg);
      } else {
        console.log("Not sending rpc message because websocket is down");
      }
    };
  }

  log(message) {
    this.logs.push(message);
  }

  sleep(ms) {
    return new Promise((resolve, reject) => setTimeout(resolve, ms));
  }

  wsOnclose(ev) {
    this.log(`WebSocket closed:${ev}`);
    console.log("WebSocket closed", ev);
    // Reload the page on websocket close to start with a clean state
    setTimeout(() => location.reload(), 1000);
  }

  wsOnOpen() {
    this.log("WebSocket connected");
    this.untilWebSocketReady.resolve(true);
  }

  subscribe(key, cb) {
    this.jrpc.on(key, ["value"], value => cb(value));
    return this.jrpc.call("subscribe", [key]);
  }

  unsubscribe(key, cb) {
    this.jrpc.off(key);
    return this.jrpc.call("unsubscribe", [key]);
  }

  setX(value) {
    return this.jrpc.call("set_x", [value]);
  }

  setY(value) {
    return this.jrpc.call("set_y", [value]);
  }

  async getTelemetry() {
    return await this.jrpc.call("get_telemetry");
  }

  async getSubscriptions() {
    return await this.jrpc.call("get_subscriptions");
  }

  dispose() {
    //not implemented
  }
}
