export default class VirtualCartesianStick {
  cartesianMax = 1;

  cartesianMin = -1;

  pressed = false;

  autoReturnToCenter = true;

  outerLineWidth = 6;

  outerStrokeColor = 'hsla(0,0%,100%,0.7)';

  innerFillColor = 'hsla(0,0%,100%,0.7)';

  innerLineWidth = 10;

  innerStrokeColor = '#42a5f5';

  boundTouchStart = this.onTouchStart.bind(this);

  boundTouchMove = this.onTouchMove.bind(this);

  boundTouchEnd = this.onTouchEnd.bind(this);

  boundMouseDown = this.onMouseDown.bind(this);

  boundMouseMove = this.onMouseMove.bind(this);

  boundMouseUp = this.onMouseUp.bind(this);

  isTouchable = document.documentElement.ontouchstart;

  onMouseUp() {
    this.pressed = false;
    if (this.autoReturnToCenter) {
      this.movedX = this.centerX;
      this.movedY = this.centerY;
    }
    this.clearCanvas();
    this.draw();
  }

  clearCanvas() {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }

  draw() {
    this.drawOuter();
    this.drawInner();
  }

  toggleAutoReturnToCenter() {
    this.autoReturnToCenter = !this.autoReturnToCenter;
  }

  get isBodyOffer() {
    return this.canvas.offsetParent.tagName.toUpperCase() === 'BODY';
  }

  drawOuter() {
    this.context.beginPath();
    this.context.arc(this.centerX, this.centerY, this.outerRadius, 0, this.circumference, false);
    this.context.lineWidth = this.outerLineWidth;
    this.context.strokeStyle = this.outerStrokeColor;
    this.context.stroke();
  }

  drawInner() {
    this.context.beginPath();
    if (this.movedX < this.innerRadius) {
      this.movedX = this.maxMoveStick;
    }
    if (this.movedX + this.innerRadius > this.canvas.width) {
      this.movedX = this.canvas.width - this.maxMoveStick;
    }
    if (this.movedY < this.innerRadius) {
      this.movedY = this.maxMoveStick;
    }
    if (this.movedY + this.innerRadius > this.canvas.height) {
      this.movedY = this.canvas.height - this.maxMoveStick;
    }

    this.context.arc(this.movedX, this.movedY, this.innerRadius, 0, this.circumference, false);

    this.addArrows();

    this.addInnerGradient();
  }

  addInnerGradient() {
    const canvasGradient = this.context.createRadialGradient(
      this.centerX,
      this.centerY,
      5,
      this.centerX,
      this.centerY,
      200,
    );

    canvasGradient.addColorStop(0, '#42a5f5');
    canvasGradient.addColorStop(1, '#0469ba');

    this.context.fillStyle = canvasGradient;
    this.context.fill();
    this.context.lineWidth = this.innerLineWidth;
    this.context.strokeStyle = this.innerStrokeColor;
    this.context.stroke();
  }

  addArrows() {
    const sWidth = this.canvas.width;
    const sHeight = this.canvas.height / 2 - 60;
    const path = new Path2D();
    path.moveTo((sWidth / 2) + 20, sHeight / 2);
    path.lineTo((sWidth / 2), (sHeight / 2) - 20);
    path.lineTo((sWidth / 2) - 20, sHeight / 2);
    this.context.fillStyle = this.innerStrokeColor;
    this.context.fill(path);
  }

  onTouchStart() {
    this.pressed = true;
  }

  handleMoveOffset() {
    if (this.isBodyOffer) {
      this.movedX -= this.canvas.offsetLeft;
      this.movedY -= this.canvas.offsetTop;
    } else {
      this.movedX -= this.canvas.offsetParent.offsetLeft;
      this.movedY -= this.canvas.offsetParent.offsetTop;
    }
  }

  onTouchMove(event) {
    event.preventDefault();
    if (this.pressed && event.targetTouches[0].target === this.canvas) {
      this.movedX = event.targetTouches[0].pageX;
      this.movedY = event.targetTouches[0].pageY;
      this.handleMoveOffset();
      this.clearCanvas();
      this.draw();
    }
  }

  onTouchEnd() {
    this.pressed = false;
    if (this.autoReturnToCenter) {
      this.movedX = this.centerX;
      this.movedY = this.centerY;
    }
    this.clearCanvas();
    this.draw();
  }

  onMouseDown() {
    this.pressed = true;
  }

  onMouseMove(event) {
    if (this.pressed) {
      this.pageX = event.pageX;
      this.pageY = event.pageY;
      this.movedX = event.pageX;
      this.movedY = event.pageY;
      this.handleMoveOffset();
      this.clearCanvas();
      this.draw();
    }
  }

  constructor({ container }) {
    this.canvas = document.createElement('canvas');
    this.canvas.id = 'virtualStick';
    this.width = container.clientWidth;
    this.height = container.clientHeight;

    this.canvas.width = this.width;
    this.canvas.height = this.height;
    container.appendChild(this.canvas);
    this.context = this.canvas.getContext('2d');

    this.circumference = 2 * Math.PI;
    this.innerRadius = (this.canvas.width - (this.canvas.width / 2 + 10)) / 2;
    this.maxMoveStick = this.innerRadius + 5;
    this.outerRadius = this.innerRadius + 30;
    this.centerX = this.canvas.width / 2;
    this.centerY = this.canvas.height / 2;
    this.movedX = this.centerX;
    this.movedY = this.centerY;
    this.attachListeners();
    this.draw();
  }

  attachListeners() {
    if (this.isTouchable) {
      this.canvas.addEventListener('touchstart', this.boundTouchStart, false);
      document.addEventListener('touchmove', this.boundTouchMove, false);
      document.addEventListener('touchend', this.boundTouchEnd, false);
    } else {
      this.canvas.addEventListener('mousedown', this.boundMouseDown, false);
      document.addEventListener('mousemove', this.boundMouseMove, false);
      document.addEventListener('mouseup', this.boundMouseUp, false);
    }
  }

  removeListeners() {
    if (this.isTouchable) {
      this.canvas.removeEventListener('touchstart', this.boundTouchStart);
      document.removeEventListener('touchmove', this.boundTouchMove);
      document.removeEventListener('touchend', this.boundTouchEnd);
    } else {
      this.canvas.removeEventListener('mousedown', this.boundMouseDown);
      document.removeEventListener('mousemove', this.boundMouseMove);
      document.removeEventListener('mouseup', this.boundMouseUp);
    }
  }

  cartesianOverlapLimit(value) {
    if (value > this.cartesianMax) {
      return this.cartesianMax;
    }

    if (value < this.cartesianMin) {
      return this.cartesianMin;
    }

    return value;
  }

  get cartesianX() {
    return this.cartesianOverlapLimit(
      Number(
        (this.cartesianMax * ((this.movedY - this.centerY) / this.maxMoveStick) * -1).toFixed(4),
      ),
    );
  }

  get cartesianY() {
    return this.cartesianOverlapLimit(
      Number((this.cartesianMax * ((this.movedX - this.centerX) / this.maxMoveStick)).toFixed(4)),
    );
  }

  destroy() {
    this.removeListeners();
  }
}
