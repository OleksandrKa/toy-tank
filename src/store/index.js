import { createStore } from 'vuex';

export default createStore({
  state: () => ({
    telemetryBarPosition: null,
    showTelemetryBar: false,
    telemetryLogs: [],
    updateTelemetryLogsInterval: 200,
    clearTelemetryLogsEvery: 100,
    telemetryLogsCount: 0,
  }),
  mutations: {
    telemetryBarPosition(state, payload) {
      state.telemetryBarPosition = payload;
    },
    showTelemetryBar(state, payload) {
      state.showTelemetryBar = payload;
    },
    addTelemetryLog(state, payload) {
      if (state.telemetryLogsCount % state.clearTelemetryLogsEvery === 0) {
        state.telemetryLogs = [
          ...state.telemetryLogs.slice(
            state.clearTelemetryLogsEvery - state.clearTelemetryLogsEvery / 5,
            state.clearTelemetryLogsEvery * 2,
          ),
        ];
      }
      state.telemetryLogsCount += 1;
      state.telemetryLogs = [...state.telemetryLogs, payload];
    },
  },
  actions: {},
  modules: {},
});
